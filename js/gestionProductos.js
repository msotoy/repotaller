var productosObtenidos;

function getProductos(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
//        console.log(request.responseText);
        productosObtenidos = request.responseText;
        procesarProductos();
    }
  }

  request.open("GET", url, true);

  request.send();
}

function procesarProductos(){
  var jsonProducto = JSON.parse(productosObtenidos);
//  alert(jsonProducto.value[0].ProductName);
  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i=0; i<jsonProducto.value.length; i++){
//  console.log(jsonProducto.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    var columnaPrecio = document.createElement("td");
    var columnaStock = document.createElement("td");

    columnaNombre.innerText = jsonProducto.value[i].ProductName;
    columnaPrecio.innerText = jsonProducto.value[i].UnitPrice;
    columnaStock.innerText = jsonProducto.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}

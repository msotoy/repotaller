var clientesObtenidos;

function getClientes(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
//        console.log(request.responseText);
        clientesObtenidos = request.responseText;
        procesarClientes();
    }
  }

  request.open("GET", url, true);

  request.send();
}


function procesarClientes(){
  var jsonClientes = JSON.parse(clientesObtenidos);
//  alert(jsonProducto.value[0].ProductName);
  var divTabla = document.getElementById("divClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i=0; i<jsonClientes.value.length; i++){
//  console.log(jsonProducto.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    var columnaCiudad = document.createElement("td");
    var columnaBandera = document.createElement("td");

    columnaNombre.innerText = jsonClientes.value[i].ContactName;
    columnaCiudad.innerText = jsonClientes.value[i].City;
    columnaBandera.appendChild(getIMGBandera(jsonClientes.value[i].Country));

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}

function getIMGBandera(pais){
  var img = document.createElement("img");
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  if (pais == 'UK') pais = 'United-Kingdom';
  img.src = rutaBandera + pais + ".png";
  img.classList.add("flag");

  return img;
}
